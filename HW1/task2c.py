# scales any list of numbers to [0,9] and counts occurences 
import math, random
listA = [random.randint(0,9) for x in range(1000)]
listB = [random.gauss(5,3) for x in range(1000)]
listC = [math.exp(random.gauss(1,0.5)) for x in range(1000)]

def mymax(lis):
    max = lis[0];
    for x in lis:
        if x > max:
            max = x
    return max
def mymin(lis):
    min = lis[0];
    for x in lis:
        if x < min:
            min = x
    return min

def myHistWithRescale(lis):
    lis.sort()
    max = mymax(lis)
    min = mymin(lis)
    diff = max - min
    split = diff / 10
    where = min
    val = 0
    counts = [0,0,0,0,0,0,0,0,0,0]
    for x in lis:
        if  where + split >= x >= where:
            x = val
            counts[val] = counts[val] + 1
        else: 
            if val < 9:
                val = val + 1
            where = where + split
            x = val
            counts[val] = counts[val] + 1
    return counts
    
countA = myHistWithRescale(listA)
countB = myHistWithRescale(listB)
countC = myHistWithRescale(listC)
print(countA)
print(countB)
print(countC)