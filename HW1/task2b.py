# calculate summary statistics of array of numbers
import random, math

listA = [random.randint(0,9) for x in range(1000)]
listB = [random.gauss(5,3) for x in range(1000)]
listC = [math.exp(random.gauss(1,0.5)) for x in range(1000)]
    
def mymax(lis):
    max = lis[0];
    for x in lis:
        if x > max:
            max = x
    return max
def mymin(lis):
    min = lis[0];
    for x in lis:
        if x < min:
            min = x
    return min

def mean(lis):
    tot = 0
    for x in lis:
        tot = tot + x
    return tot / len(lis)
        
def stddev(lis):
    mean1 = mean(lis)
    vals = []
    for x in lis:
        val = (x - mean1)**2
        vals.append(val)
    meanVals = mean(vals)
    return math.sqrt(meanVals)
    
def median(lis):
    sortlis = lis
    sortlis.sort()
    if len(sortlis) % 2 != 0:
        mid = int(len(sortlis)/2)
        median = (sortlis[mid] + sortlis[mid+1])/2
    else :
        median = sortlis[int(len(sortlis)/2)]
    return median
    
def precent25(lis):
    sortlis = lis
    sortlis.sort()
    where = .25*len(lis)
    if where % 1 != 0:
        w1 = int(where)
        pret25 = (sortlis[w1] + sortlis[w1+1])/2
    else :
        pret25 = sortlis[int(where)]
    return pret25

def precent50(lis):
    sortlis = lis
    sortlis.sort()
    where = .5*len(lis)
    if where % 1 != 0:
        w1 = int(where)
        pret5 = (sortlis[w1] + sortlis[w1+1])/2
    else :
        pret5 = sortlis[int(where)]
    return pret5

def precent75(lis):
    sortlis = lis
    sortlis.sort()
    where = .75*len(lis)
    if where % 1 != 0:
        w1 = int(where)
        pret75 = (sortlis[w1] + sortlis[w1+1])/2
    else :
        pret75 = sortlis[int(where)]
    return pret75

#calculate: max,min,mean,std dev,median, 75,50,25
def summaryStatistics(lis):
    results = {}
    results['max'] = mymax(lis)
    results['min'] = mymin(lis)
    results['mean'] = mean(lis)
    results['stddev'] = stddev(lis)
    results['median'] = median(lis)
    results['25%'] = precent25(lis)
    results['50%'] = precent50(lis)
    results['75%'] = precent75(lis)
    return results

ssA = summaryStatistics(listA)
ssB = summaryStatistics(listB)
ssC = summaryStatistics(listC)
print(ssA)
print(ssB)
print(ssC)