#Task 4
import numpy as np

x = np.array( [[1, 2, 3, 4],
               [5, 6, 7, 8],
               [9, 10,11,12],
               [13,14,15,16]])

print('a) ' + str(x[:,2]))
print('b) ' + str(x[-1,:2]))
print('c) ' + str(x[:,[True, False, False, True]]))
print('d) ' + str(x[0:2,0:2]))
print('e) ' + str(x[[0,1,2],[0,1,2]]))
print('f) ' + str(x[0]**2))
print('g) ' + str(x.max(axis=1)))
print('h) ' + str(x[:2,:2]+x[:2,2:]))
print('i) ' + str(x[:2,:3].T))
print('j) ' + str(x[:2,:3].reshape((3,2))))
print('k) ' + str(x[:,:2].dot([1,1])))
print('l) ' + str(x[:,:2].dot([[3,0],[0,2]])))