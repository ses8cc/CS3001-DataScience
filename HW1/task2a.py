# task 2: merge sort a list
import random

a = [random.randint(0,9) for x in range(10)]

def mergeSort(arr):
    ll=[]
    rl=[]
    #split array into 2
    if len(arr) > 1:
        m = int(len(arr)/2)
        ll = arr[:m] 
        rl = arr[m:]
        #recursively call
        mergeSort(ll)
        mergeSort(rl)
    
    #sorting:
    l=0
    r=0
    where=0
    
    
    for x in range(0,len(ll)+len(rl)):
        if l >= len(ll) and r < len(rl):
            arr[x]=rl[r]
            r=r+1
        elif r >= len(rl) and l < len(ll):
            arr[x]=ll[l]
            l=l+1
        else:
            if ll[l] < rl[r]:
                arr[x]=ll[l]
                l=l+1
            else:
                arr[x]=rl[r]
                r=r+1
            
    return arr

b = mergeSort(a)
print(b)
print('mergeSort is', 'correct' if b == sorted(b) else 'incorrect')