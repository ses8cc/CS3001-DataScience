# Task 3 Visualization: a)
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

black_tri_d = mlines.Line2D([], [], color='black', marker='v', linestyle='none', markersize=6, label='max')
black_tri_up = mlines.Line2D([], [], color='black', marker='^', linestyle='none', markersize=6, label='min')
blue_plus = mlines.Line2D([], [], color='blue', marker='+', linestyle='none', markersize=6, label='mean')
blue_x = mlines.Line2D([], [], color='blue', marker='x', linestyle='none', markersize=6, label='median')
green_tri_d = mlines.Line2D([], [], color='green', marker='v', linestyle='none', markersize=6, label='mean+std')
green_tri_up = mlines.Line2D([], [], color='green', marker='^', linestyle='none', markersize=6, label='mean-std')
red_tri_d = mlines.Line2D([], [], color='red', marker='v', linestyle='none', markersize=6, label='75 prec')
red_tri_up = mlines.Line2D([], [], color='red', marker='^', linestyle='none', markersize=6, label='25 prec')

plt.legend(handles=[black_tri_d, black_tri_up, blue_plus, blue_x, green_tri_d, green_tri_up, red_tri_d, red_tri_up], 
          bbox_to_anchor=(.85, 1))

plt.plot('Data 1',ssA['max'], marker='v', color='black')
plt.plot('Data 1',ssA['min'], marker='^', color='black')
plt.plot('Data 1',ssA['mean'], marker='+', color='blue')
plt.plot('Data 1',ssA['median'], marker='x', color='blue')
plt.plot('Data 1',(ssA['mean']+ssA['stddev']), marker='v', color='green')
plt.plot('Data 1',(ssA['mean']-ssA['stddev']), marker='^', color='green')
plt.plot('Data 1',ssA['75%'], marker='v', color='red')
plt.plot('Data 1',ssA['25%'], marker='^', color='red')

plt.plot('Data 2',ssB['max'], marker='v', color='black')
plt.plot('Data 2',ssB['min'], marker='^', color='black')
plt.plot('Data 2',ssB['mean'], marker='+', color='blue')
plt.plot('Data 2',ssB['median'], marker='x', color='blue')
plt.plot('Data 2',(ssB['mean']+ssB['stddev']), marker='v', color='green')
plt.plot('Data 2',(ssB['mean']-ssB['stddev']), marker='^', color='green')
plt.plot('Data 2',ssB['75%'], marker='v', color='red')
plt.plot('Data 2',ssB['25%'], marker='^', color='red')

plt.plot('Data 3',ssC['max'], marker='v', color='black')
plt.plot('Data 3',ssC['min'], marker='^', color='black')
plt.plot('Data 3',ssC['mean'], marker='+', color='blue')
plt.plot('Data 3',ssC['median'], marker='x', color='blue')
plt.plot('Data 3',(ssC['mean']+ssC['stddev']), marker='v', color='green')
plt.plot('Data 3',(ssC['mean']-ssC['stddev']), marker='^', color='green')
plt.plot('Data 3',ssC['75%'], marker='v', color='red')
plt.plot('Data 3',ssC['25%'], marker='^', color='red')

plt.show()