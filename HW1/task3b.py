#Task 3: b)
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

red_dot = mlines.Line2D([], [], color='red', marker='o', linestyle='solid', markersize=6, label='Rescaled Data 1')
green_line = mlines.Line2D([], [], color='green', marker='|', linestyle='dashdot', markersize=6, label='Rescaled Data 2')
blue_dash = mlines.Line2D([], [], color='blue', marker='x', linestyle='dotted', markersize=6, label='Rescaled Data 3')

plt.legend(handles=[red_dot, green_line, blue_dash])

plt.plot([0,1,2,3,4,5,6,7,8,9], countA, 'ro-')
plt.plot([0,1,2,3,4,5,6,7,8,9], countB, 'g|-.')
plt.plot([0,1,2,3,4,5,6,7,8,9], countC, color='blue',marker='x',linestyle='dotted')

plt.xlabel('Value')
plt.ylabel('Frequency')

plt.show()