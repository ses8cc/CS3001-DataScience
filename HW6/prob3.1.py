import random
import numpy as np
from sklearn.linear_model import Ridge
from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn.linear_model import ElasticNet
from sklearn.preprocessing import StandardScaler
import pandas as pd

with open("cadata.txt") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

#cross validation: split into 5 parts
tot = []
for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    tot.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]), float(line[6]), float(line[7]), float(line[8])])

df_total = pd.DataFrame(tot)

# split data into training(80%) and testing (20%)
split1 = []
split2 = []
split3 = []
split4 = []
split5 = []
count = 1
for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    if count == 1:
        split1.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                    float(line[6]), float(line[7]), float(line[8])])
    elif count == 2:
        split2.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 3:
        split3.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 4:
        split4.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 5:
        split5.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
        count = 0
    count = count + 1

df1 = pd.DataFrame(split1)
df2 = pd.DataFrame(split2)
df3 = pd.DataFrame(split3)
df4 = pd.DataFrame(split4)
df5 = pd.DataFrame(split5)

res1 = pd.concat([df2, df3, df4, df5])
res2 = pd.concat([df1, df3, df4, df5])
res3 = pd.concat([df1, df2, df4, df5])
res4 = pd.concat([df1, df2, df3, df5])
res5 = pd.concat([df1, df2, df3, df4])

dataframes = [res1, res2, res3, res4, res5]
# ---------run regressions-----------
# Linear Regression (no scaling)
for frame in dataframes:
    y = frame[0]
    X = frame[[1,2,3,4,5,6,7,8]]

    # Linear Regression (no scaling)
    reg = LinearRegression().fit(X, y)
    print(reg.score(X, y))

    # Ridge Regression (no scaling)
    clf = Ridge(alpha=1.0)
    clf.fit(X, y)
    print(clf.score(X,y))

    # Lasso Regression (no scaling)
    clf = linear_model.Lasso(alpha=0.1)
    clf.fit(X, y)
    print(clf.score(X,y))

    # ElasticNet (no scaling)
    regr = ElasticNet(random_state=0)
    regr.fit(X, y)
    print(regr.score(X,y))



# ------Scaling Data-------
print("Scaled Data Regressions")

scaler = StandardScaler()
scaler.fit(df_total)
new_tot = scaler.transform(df_total)

df_total = pd.DataFrame(new_tot)

df1 = pd.DataFrame()
df2 = pd.DataFrame()
df3 = pd.DataFrame()
df4 = pd.DataFrame()
df5 = pd.DataFrame()

count = 1
for index, row in df_total.iterrows():
    if count == 1:
        df1 = df1.append(row)
    elif count == 2:
        df2 = df2.append(row)
    elif count == 3:
        df3 = df3.append(row)
    elif count == 4:
        df4 = df4.append(row)
    elif count == 5:
        df5 = df5.append(row)
        count = 0
    count = count + 1

res1 = pd.concat([df2, df3, df4, df5])
res2 = pd.concat([df1, df3, df4, df5])
res3 = pd.concat([df1, df2, df4, df5])
res4 = pd.concat([df1, df2, df3, df5])
res5 = pd.concat([df1, df2, df3, df4])

dataframes = [res1, res2, res3, res4, res5]
# Linear Regression (no scaling)
for frame in dataframes:
    y = frame[0]
    X = frame[[1, 2, 3, 4, 5, 6, 7, 8]]

    # Linear Regression (no scaling)
    reg = LinearRegression().fit(X, y)
    print(reg.score(X, y))

    # Ridge Regression (no scaling)
    clf = Ridge(alpha=1.0)
    clf.fit(X, y)
    print(clf.score(X,y))

    # Lasso Regression (no scaling)
    clf = linear_model.Lasso(alpha=0.1)
    clf.fit(X, y)
    print(clf.score(X,y))

    # ElasticNet (no scaling)
    regr = ElasticNet(random_state=0)
    regr.fit(X, y)
    print(regr.score(X,y))