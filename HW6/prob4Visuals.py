from __future__ import print_function
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge
from sklearn import linear_model
from sklearn.linear_model import ElasticNet
import pandas as pd

with open("cadata.txt") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

#cross validation: split into 5 parts
tot = []
for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    tot.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]), float(line[6]), float(line[7]), float(line[8])])

df_total = pd.DataFrame(tot)

# split data into training(80%) and testing (20%)
split1 = []
split2 = []
split3 = []
split4 = []
split5 = []
count = 1
for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    if count == 1:
        split1.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                    float(line[6]), float(line[7]), float(line[8])])
    elif count == 2:
        split2.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 3:
        split3.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 4:
        split4.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 5:
        split5.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
        count = 0
    count = count + 1

df1 = pd.DataFrame(split1)
df2 = pd.DataFrame(split2)
df3 = pd.DataFrame(split3)
df4 = pd.DataFrame(split4)
df5 = pd.DataFrame(split5)

res1 = pd.concat([df2, df3, df4, df5])
res2 = pd.concat([df1, df3, df4, df5])
res3 = pd.concat([df1, df2, df4, df5])
res4 = pd.concat([df1, df2, df3, df5])
res5 = pd.concat([df1, df2, df3, df4])

dataframes = [res1, res2, res3, res4, res5]
testers = [df1, df2, df3, df4, df5]
# ---------run regressions-----------
# Linear Regression (no scaling)
for i in range(0, 5): # frame, tester in dataframes, testers:
    y_train = dataframes[i][0]
    X_train = dataframes[i][[1,2,3,4,5,6,7,8]]
    y_test = testers[i][0]
    X_test = testers[i][[1,2,3,4,5,6,7,8]]

    print("Ridge Regression")

    lr = Ridge(alpha=0.2)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = Ridge(alpha=0.4)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = Ridge(alpha=0.6)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = Ridge(alpha=0.8)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = Ridge(alpha=1.0)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    print("Lasso Regression")

    lr = linear_model.Lasso(alpha = 0.2)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = linear_model.Lasso(alpha= 0.4)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = linear_model.Lasso(alpha=0.6)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = linear_model.Lasso(alpha=0.8)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = linear_model.Lasso(alpha=1.0)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    print("ElasticNet Regression")

    lr = ElasticNet(random_state=1)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

    lr = ElasticNet(random_state=None)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))

X= [0.2,0.4,0.6,0.8,1.0]
y=[0.6428010072000161,
0.6428011520583968,
0.6428012966138147,
0.6428014408663515,
0.6428015848160894]

plt.scatter(X, y)
plt.title("Ridge Regression")
plt.show()

X= [0.2,0.4,0.6,0.8,1.0]
y=[0.642800917495608,
0.6428009729100232,
0.6428010282817704,
0.6428010836110556,
0.6428011388975561]

plt.scatter(X, y)
plt.title("Lasso Regression")
plt.show()

X= ['1', 'None']
y=[0.6077500467768935,
0.6077500467768935]

plt.scatter(X, y)
plt.title("ElasticNet Regression")
plt.show()