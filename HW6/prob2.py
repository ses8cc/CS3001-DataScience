import numpy as np
import matplotlib.pyplot as plt

with open("cadata.txt") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

yy = []  # avg house val
f1 = []  # avg income
f2 = []  # houseing average age
f3 = []  # avg rooms
f4 = []  # avg bedrooms
f5 = []  # population
f6 = []  # households
f7 = []  # lat
f8 = []  # long

for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    yy.append(float(line[0]))
    f1.append(float(line[1]))
    f2.append(float(line[2]))
    f3.append(float(line[3]))
    f4.append(float(line[4]))
    f5.append(float(line[5]))
    f6.append(float(line[6]))
    f7.append(float(line[7]))
    f8.append(float(line[8]))

# scatter plot
plt.scatter(f1, yy)
plt.title("Average Housing Value for Average Income")
plt.show()

plt.scatter(f2, yy)
plt.title("Average Housing Value for Average Housing Age")
plt.show()

plt.scatter(f3, yy)
plt.title("Average Housing Value for Average Rooms")
plt.show()

plt.scatter(f4, yy)
plt.title("Average Housing Value for Average Bedrooms")
plt.show()

plt.scatter(f5, yy)
plt.title("Average Housing Value for Population")
plt.show()

plt.scatter(f6, yy)
plt.title("Average Housing Value for Households")
plt.show()

plt.scatter(f7, yy)
plt.title("Average Housing Value for Latitudes")
plt.show()

plt.scatter(f8, yy)
plt.title("Average Housing Value for Longitudes")
plt.show()
