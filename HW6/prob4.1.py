from __future__ import print_function

from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC
from sklearn.linear_model import Ridge
from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn.linear_model import ElasticNet
from sklearn.preprocessing import StandardScaler
import pandas as pd

with open("cadata.txt") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

#cross validation: split into 5 parts
tot = []
for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    tot.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]), float(line[6]), float(line[7]), float(line[8])])

df_total = pd.DataFrame(tot)

# split data into training(80%) and testing (20%)
split1 = []
split2 = []
split3 = []
split4 = []
split5 = []
count = 1
for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    if count == 1:
        split1.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                    float(line[6]), float(line[7]), float(line[8])])
    elif count == 2:
        split2.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 3:
        split3.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 4:
        split4.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
    elif count == 5:
        split5.append([float(line[0]), float(line[1]), float(line[2]), float(line[3]), float(line[4]), float(line[5]),
                       float(line[6]), float(line[7]), float(line[8])])
        count = 0
    count = count + 1

df1 = pd.DataFrame(split1)
df2 = pd.DataFrame(split2)
df3 = pd.DataFrame(split3)
df4 = pd.DataFrame(split4)
df5 = pd.DataFrame(split5)

res1 = pd.concat([df2, df3, df4, df5])
res2 = pd.concat([df1, df3, df4, df5])
res3 = pd.concat([df1, df2, df4, df5])
res4 = pd.concat([df1, df2, df3, df5])
res5 = pd.concat([df1, df2, df3, df4])

dataframes = [res1, res2, res3, res4, res5]
testers = [df1, df2, df3, df4, df5]
# ---------run regressions-----------
# Linear Regression (no scaling)
for i in range(0, 5): # frame, tester in dataframes, testers:
    y_train = dataframes[i][0]
    X_train = dataframes[i][[1,2,3,4,5,6,7,8]]
    y_test = testers[i][0]
    X_test = testers[i][[1,2,3,4,5,6,7,8]]

    tuned_parameters = [
        {'fit_intercept': [True, False] , 'normalize': [True, False] , 'copy_X': [True, False]}
    ]

    print("Tuning hyper-parameters - Linear Regression")

    lr = LinearRegression()
    lr = GridSearchCV(lr, tuned_parameters, cv=5)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))
    print("Best parameters set found on development set:")
    print()
    print(lr.best_params_)
    print(lr.best_estimator_.coef_)

    tuned_parameters = [
        {'alpha': [0.0, 1.0], 'fit_intercept': [True, False], 'normalize': [True, False], 'copy_X': [True, False]}
    ]

    print("Tuning hyper-parameters - Ridge Regression")

    lr = Ridge()
    lr = GridSearchCV(lr, tuned_parameters, cv=5)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))
    print("Best parameters set found on development set:")
    print()
    print(lr.best_params_)
    print(lr.best_estimator_.coef_)

    tuned_parameters = [
        {'alpha': [0.1, 1.0]}
    ]

    print("Tuning hyper-parameters - Lasso Regression")

    lr = linear_model.Lasso()
    lr = GridSearchCV(lr, tuned_parameters, cv=5)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))
    print("Best parameters set found on development set:")
    print()
    print(lr.best_params_)
    print(lr.best_estimator_.coef_)

    tuned_parameters = [
        {'random_state': [1, None]}
    ]

    print("Tuning hyper-parameters - ElasticNet Regression")

    lr1 = ElasticNet()
    lr = GridSearchCV(lr1, tuned_parameters, cv=5)
    lr.fit(X_train, y_train)
    print(lr.score(X_test, y_test))
    print("Best parameters set found on development set:")
    print()
    print(lr.best_params_)
    print(lr.best_estimator_.coef_)
