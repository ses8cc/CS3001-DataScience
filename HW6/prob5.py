from __future__ import print_function
import matplotlib.pyplot as plt


X= ['avg income', 'houseing average age', 'avg rooms', 'avg bedrooms', 'population', 'households',
    'lat', 'long']
y=[3.99584979e+04, 1.14218355e+03, -7.37516229e+00, 1.11766435e+02, -4.49573406e+01, 6.19319351e+01, -4.27597497e+04, -4.27071266e+04]

plt.scatter(X, y)
plt.title("Coefficent importance from Linear Regression")
plt.show()


y=[ 3.99584979e+04,  1.14218355e+03, -7.37516229e+00,  1.11766435e+02,
 -4.49573406e+01,  6.19319351e+01, -4.27597497e+04, -4.27071266e+04]


plt.scatter(X, y)
plt.title("Coefficent importance from Ridge Regression")
plt.show()


y=[ 3.99585148e+04,  1.14218895e+03, -7.37518993e+00,  1.11765884e+02,
 -4.49573587e+01,  6.19328064e+01, -4.27594009e+04, -4.27067527e+04]


plt.scatter(X, y)
plt.title("Coefficent importance from Lasso Regression")
plt.show()


y=[ 3.66100960e+04,  1.55682274e+03, -2.97424157e+00,  3.32363447e+01,
 -4.95757129e+01,  1.42469210e+02, -1.74096838e+04, -1.52510832e+04]


plt.scatter(X, y)
plt.title("Coefficent importance from ElasticNet Regression")
plt.show()