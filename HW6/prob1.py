import numpy as np
import matplotlib.pyplot as plt

with open("cadata.txt") as f:
    content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
content = [x.strip() for x in content]

yy = []  # avg house val
f1 = []  # avg income
f2 = []  # houseing average age
f3 = []  # avg rooms
f4 = []  # avg bedrooms
f5 = []  # population
f6 = []  # households
f7 = []  # lat
f8 = []  # long

for x in content:
    x = x.replace(" -", "  -")
    line = x.split("  ")
    yy.append(float(line[0]))
    f1.append(float(line[1]))
    f2.append(float(line[2]))
    f3.append(float(line[3]))
    f4.append(float(line[4]))
    f5.append(float(line[5]))
    f6.append(float(line[6]))
    f7.append(float(line[7]))
    f8.append(float(line[8]))

# sort each feature and y
syy = yy
sf1 = f1
sf2 = f2
sf3 = f3
sf4 = f4
sf5 = f5
sf6 = f6
sf7 = f7
sf8 = f8

syy.sort()
sf1.sort()
sf2.sort()
sf3.sort()
sf4.sort()
sf5.sort()
sf6.sort()
sf7.sort()
sf8.sort()

# house value
plt.boxplot(syy)
plt.title("Average House Value")
plt.show()

plt.boxplot(sf1)
plt.title("Average Income")
plt.show()

plt.boxplot(sf2)
plt.title("Housing Average Age")
plt.show()

plt.boxplot(sf3)
plt.title("Average Rooms")
plt.show()

plt.boxplot(sf4)
plt.title("Average Bedrooms")
plt.show()

plt.boxplot(sf5)
plt.title("Population")
plt.show()

plt.boxplot(sf6)
plt.title("Households")
plt.show()

plt.boxplot(sf7)
plt.title("Latitude")
plt.show()

plt.boxplot(sf8)
plt.title("Longitude")
plt.show()
