import urllib.request
from bs4 import BeautifulSoup
from urllib.parse import urljoin
import time
import hashlib
import re


def crawl(seeds, limit=10):
    frontier = seeds
    visited_urls = set()
    count = 0

    # crawl urls in seeds
    for crawl_url in frontier:
        print("Crawling: ", crawl_url)
        visited_urls.add(crawl_url)

        try:
            resp = urllib.request.urlopen(crawl_url)
        except:
            print("Could not access ", crawl_url)
            continue

        content_type = resp.info().get("Content-Type")
        if not content_type.startswith('text/html'):
            print("Skipping %'s content" % content_type)
            continue

        contents = str(resp.read(), encoding='utf8')
        soup = BeautifulSoup(contents, features='html.parser')

        filename = 'pages/' + hashlib.md5(crawl_url.encode()).hexdigest() + '.html'
        pages = open(filename, "w")
        pages.write(contents)
        pages.close()
        print('Added file ' + filename)
        count = count + 1

        if count >= limit:
            break

        discovered_urls = set()
        links = soup('a')
        for link in links:
            if 'href' in dict(link.attrs):
                url = urljoin(crawl_url, link['href'])
                if url[0:4] == 'http' and url not in visited_urls and url not in discovered_urls and url \
                        not in frontier:
                    p = re.compile('(.*\.mst\.edu)')
                    if p.match(url):
                        print(url)
                        discovered_urls.add(url)

        frontier += discovered_urls
        time.sleep(2)


crawl(["https://www.mst.edu"], 3)
